package com.dhanjyothi;
//http://java.candidjava.com/tutorial/spring-custom-validator-example-with-Validator-interface.htm
//http://www.bogotobogo.com/Java/tutorials/Spring-Boot/Spring-Boot-HelloWorld-with-Maven.php(deploy as jar)
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.Validator;
 //https://www.devglan.com/spring-boot/spring-boot-h2-database-example
//https://www.javaguides.net/2019/08/spring-boot-crud-rest-api-spring-data-jpa-h2-database-example.html
@SpringBootApplication
public class Application {
     
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
 
 
}